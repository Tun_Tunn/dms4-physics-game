using UnityEngine;

public class EndlessVerticalScroll : MonoBehaviour
{
    public float scrollSpeed = 0.5f;
    public GameObject background;  // Public variable to assign the background GameObject

    private Vector3 startPosition;
    private float spriteHeight;

    void Start()
    {
        // Ensure there is a background assigned
        if (background == null)
        {
            Debug.LogError("Background GameObject is not assigned in the inspector!");
            return;
        }

        // Setup initial values using the background's position and size
        startPosition = background.transform.position;
        spriteHeight = background.GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void Update()
    {
        if (background != null)
        {
            float newPosition = Mathf.Repeat(Time.time * scrollSpeed, spriteHeight);
            background.transform.position = startPosition + Vector3.down * newPosition;
        }
    }
}

using System.Collections.Generic;
using UnityEngine;

public class InfiniteSpawner : MonoBehaviour
{
    public List<GameObject> objectsToSpawn; // Danh sách các GameObject để nhân bản
    public GameObject diedObject; // Đối tượng "died object" để nhân bản giữa các objectsToSpawn
    public float minDeltaY = 2.0f, maxDeltaY = 10.0f; // Khoảng cách Y cho objectsToSpawn
    public float minDiedObjectDeltaY = 1.0f, maxDiedObjectDeltaY = 5.0f; // Khoảng cách Y cho diedObject
    public float minDeltaX = 1.0f, maxDeltaX = 5.0f; // Khoảng cách X cho objectsToSpawn
    public float minDiedObjectDeltaX = 0.5f, maxDiedObjectDeltaX = 2.5f; // Khoảng cách X cho diedObject
    public float spawnInterval = 2.0f; // Thời gian giữa các lần nhân bản
    public float spawnRadius = 2.0f; // Bán kính kiểm tra vị trí để đảm bảo không trùng lắp
    public float XMin = -10.0f, XMax = 10.0f; // Giới hạn trên trục X
    private Vector3 lastSpawnPoint; // Lưu lại vị trí nhân bản cuối cùng

    void Start()
    {
        lastSpawnPoint = transform.position;
        Invoke("SpawnObject", spawnInterval);
    }

    void SpawnObject()
    {
        if (objectsToSpawn.Count == 0 || diedObject == null)
        {
            Debug.LogError("No objects to spawn or died object is null.");
            return;
        }

        Spawn(objectsToSpawn, minDeltaX, maxDeltaX, minDeltaY, maxDeltaY);
        Spawn(new List<GameObject>{diedObject}, minDiedObjectDeltaX, maxDiedObjectDeltaX, minDiedObjectDeltaY, maxDiedObjectDeltaY);
        Spawn(objectsToSpawn, minDeltaX, maxDeltaX, minDeltaY, maxDeltaY);

        Invoke("SpawnObject", spawnInterval);
    }

    void Spawn(List<GameObject> spawnList, float deltaXMin, float deltaXMax, float deltaYMin, float deltaYMax)
    {
        GameObject objectToSpawn = spawnList[Random.Range(0, spawnList.Count)];
        Vector3 potentialSpawnPoint = lastSpawnPoint;
        float deltaX;

        do
        {
            deltaX = Random.Range(deltaXMin, deltaXMax) * (Random.Range(0, 2) * 2 - 1);
            potentialSpawnPoint.x = lastSpawnPoint.x + deltaX;
        } while (Mathf.Abs(deltaX) < deltaXMin || potentialSpawnPoint.x < XMin || potentialSpawnPoint.x > XMax);

        potentialSpawnPoint.x = Mathf.Clamp(potentialSpawnPoint.x, XMin, XMax);

        if (!Physics.CheckSphere(potentialSpawnPoint, spawnRadius))
        {
            Instantiate(objectToSpawn, potentialSpawnPoint, Quaternion.identity);
            lastSpawnPoint = potentialSpawnPoint;
            lastSpawnPoint.y += Random.Range(deltaYMin, deltaYMax);
        }
    }
}

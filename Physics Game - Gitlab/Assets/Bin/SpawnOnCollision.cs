using UnityEngine;

public class SpawnOnCollision : MonoBehaviour
{
    public GameObject player;
    public GameObject gameobject1; // GameObject 1
    public GameObject gameobject2; // GameObject 2 để spawn
    public float ySpawn; // Thông số Y spawn

    private void Start()
    {
        if (gameobject1 == null || gameobject2 == null)
        {
            Debug.LogError("Please assign both gameobject1 and gameobject2 in the inspector.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && gameobject1 != null && gameobject2 != null)
        {
            Vector3 spawnPosition = new Vector3(gameobject1.transform.position.x, ySpawn, gameobject1.transform.position.z);
            Instantiate(gameobject2, spawnPosition, Quaternion.identity);
            Debug.Log("Spawn Work: gameobject2 đã được spawn tại vị trí " + spawnPosition);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public GameObject gameobject; // Chưa rõ gameobject này để làm gì, bạn có thể xóa nếu không cần thiết.

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.parent = transform; // Gán Player làm con của MovingPlatform khi va chạm
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.parent = null; // Bỏ gán Player làm con của MovingPlatform khi kết thúc va chạm
        }
    }
}

using UnityEngine;

public class Collectible : MonoBehaviour
{
    public int scoreValue = 1; // Giá trị điểm khi thu thập

    private void Awake()
    {
        // Uncomment the following line if you want the collectible to persist across scenes
        // DontDestroyOnLoad(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Kiểm tra xem đối tượng chạm vào có phải là người chơi không
        {
            CollectibleManager.Instance.CollectItem(this); // Gọi hàm thu thập từ CollectibleManager
        }
    }
}

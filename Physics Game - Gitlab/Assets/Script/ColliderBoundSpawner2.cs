using UnityEngine;
using System.Collections.Generic;

public class ColliderBoundSpawner2 : MonoBehaviour
{
    public ObjectPooler objectPool;
    public GameObject area;
    public List<GameObject> objectsToSpawn;
    public float minDeltaY = 2.0f, maxDeltaY = 10.0f;
    public float minDeltaX = 1.0f, maxDeltaX = 5.0f;

    public int minObjects = 10, maxObjects = 15;

    void Start()
    {
        if (objectPool == null)
        {
            Debug.LogError("ObjectPooler reference is required for ColliderBoundSpawner2.");
            return;
        }
        if (area == null)
        {
            Debug.LogError("An area GameObject is required.");
            return;
        }
        Collider collider = area.GetComponent<Collider>();
        if (collider == null)
        {
            Debug.LogError("The area GameObject must have a Collider component.");
            return;
        }

        Bounds bounds = collider.bounds;
        int objectCount = Random.Range(minObjects, maxObjects + 1);
        for (int i = 0; i < objectCount; i++)
        {
            SpawnObjectWithinBounds(bounds);
        }
    }

    void SpawnObjectWithinBounds(Bounds bounds)
    {
        Vector3 spawnPosition = GetNextSpawnPosition(bounds);
        if (objectsToSpawn.Count == 0)
        {
            Debug.LogWarning("No objects to spawn are defined.");
            return;
        }
        string tag = objectsToSpawn[Random.Range(0, objectsToSpawn.Count)].tag;
        GameObject pooledObject = objectPool.SpawnFromPool(tag, spawnPosition, Quaternion.identity);
        if (pooledObject == null)
        {
            Debug.LogWarning("No pooled object available for tag: " + tag);
        }
    }

    Vector3 GetNextSpawnPosition(Bounds bounds)
    {
        float spawnX = Random.Range(bounds.min.x + minDeltaX, bounds.max.x - maxDeltaX);
        float spawnY = Random.Range(bounds.min.y + minDeltaY, bounds.max.y - maxDeltaY);
        return new Vector3(spawnX, spawnY, 0);
    }
}

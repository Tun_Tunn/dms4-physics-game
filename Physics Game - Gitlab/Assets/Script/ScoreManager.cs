using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public TMP_Text scoreText;
    public TMP_Text highScoreText; // Text to display the high score
    public HeightManager heightManager; // Reference to HeightManager
    public List<Vector2> heightScoreMultipliers; // List to store height and corresponding score multipliers
    private int score = 0;
    private int highScore = 0; // Variable to store the high score

    // Public property to access the current score safely
    public int CurrentScore
    {
        get { return score; }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnEnable()
    {
        // Đảm bảo scoreText và highScoreText được thiết lập đúng
        if (scoreText == null)
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<TMP_Text>();
        }
        if (highScoreText == null)
        {
            highScoreText = GameObject.Find("HighScoreText").GetComponent<TMP_Text>();
        }

        // Cập nhật lại điểm số
        UpdateScoreText();
        UpdateHighScore();
    }

    void Start()
    {
        LoadHighScore();
        UpdateScoreText();
    }

    public void AddScore(int newScoreValue)
    {
        float height = heightManager.CurrentHeight;
        int scoreMultiplier = 1;

        foreach (Vector2 heightMultiplier in heightScoreMultipliers)
        {
            if (height > heightMultiplier.x)
            {
                scoreMultiplier = (int)heightMultiplier.y;
            }
        }

        score += newScoreValue * scoreMultiplier;
        UpdateScoreText();
        UpdateHighScore();
    }

    void UpdateScoreText()
    {
        if (scoreText != null)
        {
            scoreText.text = "Score: " + score;
        }
    }

    void UpdateHighScore()
    {
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HighScore", highScore);
            PlayerPrefs.Save();
        }
        if (highScoreText != null)
        {
            highScoreText.text = "High Score: " + highScore;
        }
    }

    void LoadHighScore()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        if (highScoreText != null)
        {
            highScoreText.text = "High Score: " + highScore;
        }
    }

    public int GetHighScore()
    {
        return highScore;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Đảm bảo scoreText và highScoreText được thiết lập đúng sau khi tải lại cảnh
        if (scoreText == null)
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<TMP_Text>();
        }
        if (highScoreText == null)
        {
            highScoreText = GameObject.Find("HighScoreText").GetComponent<TMP_Text>();
        }

        // Cập nhật lại điểm số
        UpdateScoreText();
        UpdateHighScore();
    }
}

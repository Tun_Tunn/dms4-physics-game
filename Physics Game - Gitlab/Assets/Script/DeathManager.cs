using UnityEngine;
using TMPro;

public class DeathManager : MonoBehaviour
{
    public AudioSource audioSource; // AudioSource để phát âm thanh
    public AudioClip deathSound; // AudioClip cho âm thanh chết
    public GameOverManager gameOverManager; // Gắn tham chiếu tới GameOverManager

    public void HandleDeath()
    {
        if (audioSource != null && deathSound != null)
        {
            audioSource.PlayOneShot(deathSound); // Phát âm thanh chết
        }
        if (gameOverManager != null)
        {
            gameOverManager.TriggerGameOver(); // Kích hoạt game over
        }
    }
}

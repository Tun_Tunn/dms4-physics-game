using UnityEngine;
using UnityEngine.UI; // Thư viện cần thiết để sử dụng UI components như Button và Image

public class SoundToggleButton : MonoBehaviour
{
    public Sprite soundOnSprite;
    public Sprite soundOffSprite;
    public Button soundButton;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = FindObjectOfType<AudioSource>(); // Tìm và gán AudioSource trong Scene
        UpdateButtonSprite(); // Cập nhật sprite khi bắt đầu
    }

    public void ToggleSound()
    {
        audioSource.mute = !audioSource.mute; // Đảo ngược trạng thái mute của AudioSource
        UpdateButtonSprite(); // Cập nhật sprite dựa trên trạng thái mới của âm thanh
    }

    private void UpdateButtonSprite()
    {
        if (audioSource.mute)
        {
            soundButton.image.sprite = soundOffSprite; // Đặt sprite cho trạng thái âm thanh tắt
        }
        else
        {
            soundButton.image.sprite = soundOnSprite; // Đặt sprite cho trạng thái âm thanh bật
        }
    }
}

using UnityEngine;
using UnityEngine.UI;  // Thư viện để sử dụng UI components như Button

public class ButtonSound : MonoBehaviour
{
    public AudioClip sound;     // Lưu trữ AudioClip để phát
    public Button button;      // Nút sẽ gắn script này
    private AudioSource audioSource;  // AudioSource để phát âm thanh

    void Start()
    {
        button = GetComponent<Button>();  // Lấy component Button từ GameObject
        audioSource = FindObjectOfType<AudioSource>();  // Tìm AudioSource trong Scene

        if (button == null)
        {
            Debug.LogError("ButtonSound script is not attached to a button");
            return;
        }

        // Thêm listener để khi nhấn nút thì gọi hàm PlaySound
        button.onClick.AddListener(PlaySound);
    }

    void PlaySound()
    {
        if (audioSource == null || sound == null)
        {
            Debug.LogError("AudioSource or AudioClip is missing");
            return;
        }

        audioSource.PlayOneShot(sound);  // Phát âm thanh một lần
    }
}

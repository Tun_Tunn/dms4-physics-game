using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerTry : MonoBehaviour
{
    public float speed = 5.0f;
    public float jumpForce = 2.0f;
    public float maxJumpTime = 1.0f;
    private float jumpTimeCounter;
    private bool isJumping;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Đảm bảo nhân vật có Rigidbody
        jumpTimeCounter = 0;
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0.0f);
        transform.Translate(movement * speed * Time.deltaTime, Space.World);

        if (Input.GetKeyDown(KeyCode.Space) && !isJumping)
        {
            isJumping = true;
            jumpTimeCounter = maxJumpTime;
        }

        if (Input.GetKey(KeyCode.Space) && isJumping)
        {
            if (jumpTimeCounter > 0)
            {
                rb.velocity = Vector2.up * jumpForce;
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJumping = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }
    }
}

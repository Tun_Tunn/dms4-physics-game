using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject pauseMenu; // GameObject để hiển thị khi tạm dừng
    public GameObject pauseButton;
    public GameObject UISystem;
    private bool isPaused = false;

    void Start()
    {
        if (pauseMenu != null)
        {
            pauseMenu.SetActive(false); // Đảm bảo pauseMenu không hiển thị khi bắt đầu
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) // Thay KeyCode.Escape bằng phím hoặc nút bạn muốn sử dụng
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        isPaused = !isPaused;
        
        if (isPaused)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    void PauseGame()
    {
        if (pauseMenu != null)
        {
            pauseMenu.SetActive(true);
            pauseButton.SetActive(false);
            UISystem.SetActive(false);
        }

        // Tạm dừng tất cả hoạt động ngoại trừ camera
        Time.timeScale = 0f;
    }

    void ResumeGame()
    {
        if (pauseMenu != null)
        {
            pauseMenu.SetActive(false);
            pauseButton.SetActive(true);
            UISystem.SetActive(true);
        }

        // Tiếp tục tất cả hoạt động
        Time.timeScale = 1f;
    }

    public void RestartGame()
    {
        // Đặt lại Time.timeScale về 1 để tiếp tục game
        Time.timeScale = 1;
        // Tải lại scene hiện tại để bắt đầu lại trò chơi
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

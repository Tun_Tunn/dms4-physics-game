using UnityEngine;

public class QuitGame : MonoBehaviour
{
    // This method is called when the quit button is pressed
    public void Quit()
    {
        // Log message to console (only shows in the editor)
        Debug.Log("Quit game requested");

        // Quit the application
        Application.Quit();

        // If running in the Unity editor, stop playing the scene
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }
}

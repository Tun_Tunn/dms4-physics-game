using UnityEngine;

public class CollectibleManager : MonoBehaviour
{
    public static CollectibleManager Instance; // Singleton instance
    public AudioSource audioSource; // AudioSource to play collect sound
    public AudioClip collectSound; // AudioClip for collect sound

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        audioSource = GetComponent<AudioSource>(); // Get the AudioSource component
    }

    // Phương thức để xử lý khi thu thập một đối tượng
    public void CollectItem(Collectible collectible)
    {
        if (collectible != null)
        {
            ScoreManager.instance.AddScore(collectible.scoreValue); // Tăng điểm cho người chơi
            audioSource.PlayOneShot(collectSound); // Play collect sound
            Destroy(collectible.gameObject); // Hủy đối tượng thu thập
        }
    }
}

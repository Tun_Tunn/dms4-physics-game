using UnityEngine;

public class VerticalSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;  
    public Transform spawnPrototype;  
    public int numberOfObjects;  
    public float verticalSpacing;
    public Vector3 spawnPosition;

    void Start()
    {
        SpawnObjects();
    }

    void SpawnObjects()
    {
        if (objectToSpawn == null || spawnPrototype == null)
        {
            Debug.LogError("ObjectToSpawn or SpawnPrototype is not assigned.");
            return;
           
        }

        for (int i = 0; i < numberOfObjects; i++)
        {
            spawnPosition = new Vector3(spawnPosition.x, spawnPosition.y + verticalSpacing, spawnPosition.z);
 
            Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
            Debug.Log("spawn works" + spawnPosition.y);
        }
    }
}
using UnityEngine;
using TMPro;

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverUI; // UI game over
    public GameObject pauseButton;
    public GameObject UISystem;
    public TMP_Text scoreText; // Text để hiển thị điểm số
    public TMP_Text heightText; // Text để hiển thị độ cao
    public TMP_Text highScoreText; // Text để hiển thị high score

    private void Start()
    {
        pauseButton.SetActive(true);
        UISystem.SetActive(true);
        gameOverUI.SetActive(false); // Ẩn UI game over khi bắt đầu
    }

    void OnEnable()
    {
        // Đảm bảo các tham chiếu được thiết lập đúng
        if (scoreText == null)
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<TMP_Text>();
        }
        if (heightText == null)
        {
            heightText = GameObject.Find("HeightText").GetComponent<TMP_Text>();
        }
        if (highScoreText == null)
        {
            highScoreText = GameObject.Find("HighScoreText").GetComponent<TMP_Text>();
        }
    }

    public void TriggerGameOver()
    {
        if (pauseButton != null) pauseButton.SetActive(false);
        if (UISystem != null) UISystem.SetActive(false);
        if (gameOverUI != null) gameOverUI.SetActive(true); // Hiển thị UI game over

        // Lấy dữ liệu từ ScoreManager và HeightManager
        int score = ScoreManager.instance.CurrentScore;
        float height = HeightManager.instance.CurrentHeight * HeightManager.instance.conversionFactor;
        int highScore = ScoreManager.instance.GetHighScore();

        // Cập nhật các TextMeshProUGUI với thông tin mới
        if (scoreText != null) scoreText.text = $"Score: {score}";
        if (heightText != null) heightText.text = $"Height: {height:0} mm";
        if (highScoreText != null) highScoreText.text = $"High Score: {highScore}";

        // Dừng tất cả các hoạt động trong game
        Time.timeScale = 0;
    }

    public void RestartGame()
    {
        // Đặt lại Time.timeScale về 1 để tiếp tục game
        Time.timeScale = 1;
        // Tải lại scene hiện tại để bắt đầu lại trò chơi
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}

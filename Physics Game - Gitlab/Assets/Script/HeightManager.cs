using UnityEngine;
using TMPro;

public class HeightManager : MonoBehaviour
{
    public static HeightManager instance; // Singleton instance
    public Transform player; // Đối tượng player mà bạn muốn lấy độ cao
    public TextMeshProUGUI heightText; // Đối tượng TextMeshProUGUI để hiển thị độ cao
    public float conversionFactor = 1000f; // Hệ số chuyển đổi từ mét sang milimet
    private float initialHeight; // Độ cao khởi điểm của player

    // Property để lấy độ cao hiện tại của player tính bằng mét
    public float CurrentHeight
    {
        get
        {
            if (player == null)
            {
                FindPlayer();
            }
            return player.position.y - initialHeight;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    void OnEnable()
    {
        // Đảm bảo heightText được thiết lập đúng
        if (heightText == null)
        {
            heightText = GameObject.Find("HeightText").GetComponent<TextMeshProUGUI>();
        }

        // Tìm lại player khi object được kích hoạt
        FindPlayer();
    }

    void Start()
    {
        // Set độ cao khởi điểm để độ cao khởi đầu của player là 0 mm
        if (player != null)
        {
            initialHeight = player.position.y;
        }
    }

    void Update()
    {
        if (player != null && heightText != null)
        {
            // Chuyển đổi độ cao hiện tại từ mét sang milimet sử dụng hệ số chuyển đổi
            float heightInMm = CurrentHeight * conversionFactor;
            // Cập nhật TextMeshProUGUI với độ cao mới, sử dụng string interpolation
            heightText.text = $"Height: {heightInMm:0} mm";
        }
    }

    private void FindPlayer()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            player = playerObject.transform;
            initialHeight = player.position.y; // Cập nhật initialHeight mỗi khi tìm thấy player mới
        }
    }
}

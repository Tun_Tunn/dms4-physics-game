using UnityEngine;

public class DeathTrigger : MonoBehaviour
{
    public DeathManager deathManager; // Gắn tham chiếu tới DeathManager

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            deathManager.HandleDeath(); // Kích hoạt xử lý khi chết
        }
    }
}

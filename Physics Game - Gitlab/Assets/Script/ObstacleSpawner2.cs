using UnityEngine;
using System.Collections.Generic;

public class ObstacleSpawner2 : MonoBehaviour
{
    public GameObject area; // GameObject để xác định vùng spawn
    public GameObject[] areaAObstacles; // Mảng prefab cho Area A
    public GameObject[] areaBObstacles; // Mảng prefab cho Area B
    public GameObject[] areaCObstacles; // Mảng prefab cho Area C

    public int maxAreaAObstacles; // Số lượng tối đa của chướng ngại vật ở Area A
    public int maxAreaBObstacles; // Số lượng tối đa của chướng ngại vật ở Area B
    public int maxAreaCObstacles; // Số lượng tối đa của chướng ngại vật ở Area C

    public float minDeltaYA, maxDeltaYA; // Khoảng cách dọc tối thiểu và tối đa giữa các chướng ngại vật ở Area A
    public float minDeltaXA, maxDeltaXA; // Khoảng cách ngang tối thiểu và tối đa giữa các chướng ngại vật ở Area A
    public float minDeltaXB, maxDeltaXB; // Khoảng cách ngang tối thiểu và tối đa giữa các chướng ngại vật ở Area B
    public float minDeltaYB, maxDeltaYB; // Khoảng cách dọc tối thiểu và tối đa giữa các chướng ngại vật ở Area B

    public float minMovingDeltaX, maxMovingDeltaX; // Khoảng cách ngang tối thiểu và tối đa cho Area C
    public float movingSpeed = 2f; // Tốc độ di chuyển ngang cho Area C

    public float spawnInterval = 2f; // Khoảng thời gian giữa các lần spawn
    private float nextSpawnTime = 0f;

    private float lastSpawnY = 0f; // Theo dõi vị trí Y của chướng ngại vật được spawn gần nhất
    private float lastSpawnX = 0f; // Theo dõi vị trí X của chướng ngại vật được spawn gần nhất

    private List<GameObject> activeAreaAObstacles = new List<GameObject>();
    private List<GameObject> activeAreaBObstacles = new List<GameObject>();
    private List<GameObject> activeAreaCObstacles = new List<GameObject>();

    private bool spawnB = true; // Biến theo dõi để đảm bảo không spawn B và C cùng lúc

    void Start()
    {
        if (area == null)
        {
            Debug.LogError("ObstacleSpawner yêu cầu một GameObject area để xác định vùng spawn.");
            return;
        }

        Collider collider = area.GetComponent<Collider>();
        if (collider == null)
        {
            Debug.LogError("GameObject area yêu cầu một component Collider.");
            return;
        }

        Bounds bounds = collider.bounds;
        lastSpawnY = bounds.min.y; // Khởi tạo lastSpawnY tại vị trí Y thấp nhất trong collider
        lastSpawnX = Random.Range(bounds.min.x, bounds.max.x); // Khởi tạo lastSpawnX tại vị trí ngẫu nhiên trong bounds
    }

    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnObstacle();
            nextSpawnTime = Time.time + spawnInterval;
        }

        // Di chuyển ngang các chướng ngại vật trong Area C
        MoveAreaCObstacles();
    }

    void SpawnObstacle()
    {
        if (areaAObstacles.Length > 0 && activeAreaAObstacles.Count < maxAreaAObstacles)
        {
            // Spawn trong Area A
            SpawnAreaAObstacle();
        }

        if (activeAreaAObstacles.Count >= 2)
        {
            if (spawnB && areaBObstacles.Length > 0 && activeAreaBObstacles.Count < maxAreaBObstacles)
            {
                // Spawn trong Area B khi có ít nhất hai chướng ngại vật ở Area A và số lượng chướng ngại vật ở Area B chưa đạt tối đa
                SpawnAreaBObstacle();
                spawnB = false; // Đảm bảo không spawn C lần tiếp theo
            }
            else if (!spawnB && areaCObstacles.Length > 0 && activeAreaCObstacles.Count < maxAreaCObstacles)
            {
                // Spawn trong Area C khi có ít nhất hai chướng ngại vật ở Area A và số lượng chướng ngại vật ở Area C chưa đạt tối đa
                SpawnAreaCObstacle();
                spawnB = true; // Đảm bảo không spawn B lần tiếp theo
            }
        }
    }

    void SpawnAreaAObstacle()
    {
        Collider collider = area.GetComponent<Collider>();
        Bounds bounds = collider.bounds;

        Vector3 spawnPosition = GetNextSpawnPosition(bounds, minDeltaXA, maxDeltaXA, minDeltaYA, maxDeltaYA);
        GameObject obstacle = Instantiate(areaAObstacles[Random.Range(0, areaAObstacles.Length)], spawnPosition, Quaternion.identity);
        activeAreaAObstacles.Add(obstacle);
    }

    void SpawnAreaBObstacle()
    {
        // Lấy vị trí của hai chướng ngại vật gần nhất ở Area A
        Vector3 lastObstaclePos1 = activeAreaAObstacles[activeAreaAObstacles.Count - 1].transform.position;
        Vector3 lastObstaclePos2 = activeAreaAObstacles[activeAreaAObstacles.Count - 2].transform.position;

        // Tính toán vị trí giữa của hai chướng ngại vật ở Area A
        Vector3 middlePosition = (lastObstaclePos1 + lastObstaclePos2) / 2;

        // Điều chỉnh vị trí giữa để đảm bảo nó nằm trong bounds
        Collider collider = area.GetComponent<Collider>();
        Bounds bounds = collider.bounds;
        middlePosition.x = Mathf.Clamp(middlePosition.x, bounds.min.x, bounds.max.x);
        middlePosition.y = Mathf.Clamp(middlePosition.y, bounds.min.y, bounds.max.y);

        // Spawn chướng ngại vật ở Area B tại vị trí giữa
        GameObject obstacle = Instantiate(areaBObstacles[Random.Range(0, areaBObstacles.Length)], middlePosition, Quaternion.identity);
        activeAreaBObstacles.Add(obstacle);
    }

    void SpawnAreaCObstacle()
    {
        // Lấy vị trí của hai chướng ngại vật gần nhất ở Area A
        Vector3 lastObstaclePos1 = activeAreaAObstacles[activeAreaAObstacles.Count - 1].transform.position;
        Vector3 lastObstaclePos2 = activeAreaAObstacles[activeAreaAObstacles.Count - 2].transform.position;

        // Tính toán vị trí giữa của hai chướng ngại vật ở Area A
        Vector3 middlePosition = (lastObstaclePos1 + lastObstaclePos2) / 2;

        // Điều chỉnh vị trí giữa để đảm bảo nó nằm trong bounds
        Collider collider = area.GetComponent<Collider>();
        Bounds bounds = collider.bounds;
        middlePosition.x = Mathf.Clamp(middlePosition.x, bounds.min.x, bounds.max.x);
        middlePosition.y = Mathf.Clamp(middlePosition.y, bounds.min.y, bounds.max.y);

        // Spawn chướng ngại vật ở Area C tại vị trí giữa
        GameObject obstacle = Instantiate(areaCObstacles[Random.Range(0, areaCObstacles.Length)], middlePosition, Quaternion.identity);
        activeAreaCObstacles.Add(obstacle);

        // Thêm script để di chuyển ngang
        MovingObstacle movingObstacle = obstacle.AddComponent<MovingObstacle>();
        movingObstacle.minX = bounds.min.x;
        movingObstacle.maxX = bounds.max.x;
        movingObstacle.speed = movingSpeed;
    }

    Vector3 GetNextSpawnPosition(Bounds bounds, float minX, float maxX, float minY, float maxY)
    {
        // Random chọn hướng: 1 là phải, -1 là trái
        int xDirection = Random.Range(0, 2) * 2 - 1;

        float spawnX = lastSpawnX + Random.Range(minX, maxX) * xDirection;
        if (spawnX > bounds.max.x || spawnX < bounds.min.x)
        {
            // Nếu vượt qua bounds, thì chuyển hướng và tính lại
            xDirection *= -1;
            spawnX = lastSpawnX + Random.Range(minX, maxX) * xDirection;
        }

        float spawnY = lastSpawnY + Random.Range(minY, maxY);
        spawnY = Mathf.Min(spawnY, bounds.max.y); // Đảm bảo spawnY không vượt quá bounds
        if (spawnY > bounds.max.y)
            spawnY = bounds.max.y;
        if (spawnY < bounds.min.y)
            spawnY = bounds.min.y;

        lastSpawnX = spawnX; // Cập nhật lastSpawnX
        lastSpawnY = spawnY; // Cập nhật lastSpawnY

        return new Vector3(spawnX, spawnY, 0); // Đảm bảo tất cả các đối tượng có vị trí Z là 0
    }

    void MoveAreaCObstacles()
    {
        foreach (GameObject obstacle in activeAreaCObstacles)
        {
            MovingObstacle movingObstacle = obstacle.GetComponent<MovingObstacle>();
            if (movingObstacle != null)
            {
                movingObstacle.Move();
            }
        }
    }

    public void RemoveObstacle(GameObject obstacle, int areaType)
    {
        if (areaType == 0)
        {
            activeAreaAObstacles.Remove(obstacle);
        }
        else if (areaType == 1)
        {
            activeAreaBObstacles.Remove(obstacle);
        }
        else if (areaType == 2)
        {
            activeAreaCObstacles.Remove(obstacle);
        }
        Destroy(obstacle);
    }
}


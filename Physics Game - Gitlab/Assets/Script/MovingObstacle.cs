// MovingObstacle.cs
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float speed;
    private int direction = 1;

    void Update()
    {
        Move();
    }

    public void Move()
    {
        Vector3 position = transform.position;
        position.x += speed * direction * Time.deltaTime;

        if (position.x >= maxX)
        {
            position.x = maxX;
            direction = -1;
            Flip();
        }
        else if (position.x <= minX)
        {
            position.x = minX;
            direction = 1;
            Flip();
        }

        transform.position = position;
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1; // Đảo chiều theo trục X
        transform.localScale = scale;
    }
}

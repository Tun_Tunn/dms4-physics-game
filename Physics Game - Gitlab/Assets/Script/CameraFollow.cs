using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player; // Tham chiếu đến transform của người chơi
    public Vector2 offset = new Vector2(0f, 2f); // Khoảng cách giữa camera và người chơi

    void LateUpdate()
    {
        // Cập nhật vị trí của camera theo vị trí của người chơi
        Vector3 newPosition = new Vector3(player.position.x + offset.x, player.position.y + offset.y, transform.position.z);
        transform.position = newPosition;
    }
}

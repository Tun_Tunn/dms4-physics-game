using UnityEngine;
using UnityEngine.UI;

public class SoundManagerTO : MonoBehaviour
{
    public Sprite soundOnSprite;
    public Sprite soundOffSprite;
    public Button soundToggleButton;

    private bool isSoundOn;

    void Start()
    {
        isSoundOn = PlayerPrefs.GetInt("soundState", 1) == 1;
        UpdateSoundState();
    }

    public void ToggleSound()
    {
        isSoundOn = !isSoundOn;
        PlayerPrefs.SetInt("soundState", isSoundOn ? 1 : 0);
        PlayerPrefs.Save();
        UpdateSoundState();
    }

    private void UpdateSoundState()
    {
        AudioListener.volume = isSoundOn ? 1.0f : 0f;
        soundToggleButton.image.sprite = isSoundOn ? soundOnSprite : soundOffSprite;
    }
}

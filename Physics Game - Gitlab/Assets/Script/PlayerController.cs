using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5.0f;
    public float maxJumpHeight = 20.0f;
    public float maxThrowDistance = 20.0f;
    public float maxPressTime = 20.0f;
    private float timePressed;
    private Rigidbody rb;
    private bool isJumping;
    private float lastHorizontalInput;
    public float minX = -10.0f;
    public float maxX = 10.0f;
    public float reboundForce = 10.0f;
    public GameObject Bar;
    public Animator animator;

    public AudioSource audioSource;
    public AudioClip runSound;
    public AudioClip jumpSound;
    public AudioClip pressSound;
    public AudioClip landSound;

    void Start()
    {
        Bar.SetActive(false);
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        // animator = GetComponent<Animator>();
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        if (moveHorizontal != 0)
        {
            if (!audioSource.isPlaying || audioSource.clip != runSound)
            {
                audioSource.clip = runSound;
                audioSource.Play();
            }
            animator.SetBool("isRunning", true);
            lastHorizontalInput = moveHorizontal;
            transform.localScale = new Vector3(Mathf.Sign(moveHorizontal), 1, 1); // Flip character direction
        }
        else
        {
            animator.SetBool("isRunning", false);
        }

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0.0f);
        transform.Translate(movement * speed * Time.deltaTime, Space.World);

        if (Input.GetKeyDown(KeyCode.Space) && !isJumping && rb.velocity.y == 0)
        {
            Bar.SetActive(true);
            timePressed = 0;
            isJumping = true;
            audioSource.PlayOneShot(pressSound);
            Debug.Log("Player pressed Space to jump.");
        }

        if (Input.GetKey(KeyCode.Space) && isJumping)
        {
            Bar.SetActive(true);
            timePressed += Time.deltaTime;
            Debug.Log($"Time Pressed: {timePressed}s");
        }

        if (Input.GetKeyUp(KeyCode.Space) && isJumping)
        {
            Bar.SetActive(false);
            float jumpHeight = Mathf.Lerp(0.5f, maxJumpHeight, timePressed / maxPressTime);
            float throwDistance = Mathf.Lerp(0.5f, maxThrowDistance, timePressed / maxPressTime) * Mathf.Sign(lastHorizontalInput);
            rb.velocity = new Vector3(throwDistance, jumpHeight, rb.velocity.z);
            isJumping = false;
            audioSource.PlayOneShot(jumpSound);
            animator.SetBool("isJumping", true); 
            Debug.Log($"Jump Height: {jumpHeight}mm");
        }
        else
        {
            animator.SetBool("isJumping", false);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Rebound")
        {
            Vector3 reboundDirection = new Vector3(-lastHorizontalInput, 1, 0).normalized * reboundForce;
            rb.velocity = reboundDirection;
            Debug.Log("Player rebounded with force: " + reboundDirection);
        }
        else if (collision.gameObject.tag == "Ground")
        {
            Debug.Log("Player is grounded.");
            isJumping = false;
            animator.SetBool("isJumping", false);
            audioSource.PlayOneShot(landSound);
            
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Debug.Log("Player is not grounded.");
        }
    }
}
